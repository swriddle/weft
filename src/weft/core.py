import logging
from twisted.internet import reactor
import weft


consumptionGlobal = weft.config.ConsumptionAttr
productionGlobal = weft.config.ProductionAttr

logger = logging.getLogger(__name__)


class Action(object):
	def __init__(self, name, isolated=False):
		self.name = name
		self.attrs = {}
		self.currentIteration = 0
		self.ports = []
		self[productionGlobal] = {}
		self[consumptionGlobal] = {}

	def ready(self, context):

		waitMore = False

		for port in self.ports:
			consumptionRate = self.getConsumptionRate(port)
			productionRate = self.getProductionRate(port)
			if consumptionRate is None or productionRate is None:
				raise weft.exceptions.UnknownFireBehaviorException()
			else:
				if len(context[port]) < consumptionRate:
					waitMore = True
					break

		return not waitMore

	def getExecutor(self):
		assert False

	def getGraphvizFor(self, context):
		nodeId = context.register(self)
		return nodeId,'{} [label="{}"];\n'.format(nodeId, self.name)

	def isComposite(self):
		return False

	def getContainmentString(self):
		current = self
		path = []
		while current is not None:
			path.append(current)
			current = current.getParent()
		path.reverse()
		return '.'.join([x.name for x in path])

	def getParent(self):
		if 'parent' in self.attrs:
			return self['parent']
		else:
			return None

	def getProductionRate(self, port):
		try:
			return self[productionGlobal][port]
		except KeyError:
			return None

	def getConsumptionRate(self, port):
		try:
			return self[consumptionGlobal][port]
		except KeyError:
			return None

		productionRate = srcAction[productionGlobal][srcPort]
		consumptionRate = dstAction[consumptionGlobal][dstPort]
	def send(self, portName, sentDatum):
		# Atomic action should forward the send request to the managing
		# container.

		referTo = self.attrs['parent']
		# referTo.send(self, portName, sendDatum)
		referTo.send(self, portName, sentDatum)

	def receive(self, portName):
		# Atomic action should forward the send request to the managing
		# container.

		referTo = self.attrs['parent']
		return referTo.receive(self, portName)

	def peek(self, portName):
		referTo = self.attrs['parent']
		return referTo.peek(self, portName)

	def _addPort(self, name, type):
		self.ports.append((name, type))

	def addInputPort(self, name):
		self._addPort(name, weft.types.PortType.input)

	def addOutputPort(self, name):
		self._addPort(name, weft.types.PortType.output)

	def hasInput(self, name):
		for (_name, _type) in self.ports:
			if _name == name and _type == weft.types.PortType.input:
				return True
		return False

	def hasOutput(self, name):
		for (_name, _type) in self.ports:
			if _name == name and _type == weft.types.PortType.output:
				return True
		return False


	def setup(self):
		self.currentIteration = 0

	def wrapup(self):
		pass

	def run(self):
		self.currentIteration += 1

	def wantsToRun(self):
		# Base implementation respects the iteration limit attribute, giving an
		# integer number of iterations to stop at

		attributeName = weft.config.IterationLimitAttr
		maxIterations = weft.config.DefaultIterationLimit
		if attributeName in self.attrs.keys():
			maxIterations = self[attributeName]

		return self.currentIteration < maxIterations

	def __setitem__(self, k, v):
		self.attrs[k] = v

	def __getitem__(self, k):
		return self.attrs[k]

class Workflow(Action):
	def __init__(self, name, composite, executor):
		super(Workflow, self).__init__(name)
		self.composite = composite
		self.composite['parent'] = self
		self.executor = executor
		self.schedule = None

	def getExecutor(self):
		return self.executor

	def getGraphvizFor(self, context):

		workflowId = context.register(self)

		topLevelTemplate = 'subgraph %s { %s \n label="%s"; }'

		o = self.composite.getGraphvizFor(context)

		id, graphviz = self.composite.getGraphvizFor(context)
		allInternalGraphviz = topLevelTemplate % (workflowId, graphviz, self.name)

		return workflowId, allInternalGraphviz

	def setup(self):

		if self.executor.name == 'sdf':
			
			# Assuming SDF...
			actions, multiplicities = weft.utils.toposortAndComputeMultiplicities(self.composite)

			logger.debug('Beginning schedule generation')
			logger.debug('--ACTIONS--:')
			for action in actions:
				logger.debug('*', action.name)

			logger.debug('--MULTIPLICITIES--:')
			for (action, multiplicity) in multiplicities.items():
				logger.debug('{} -> {}'.format(action.name, multiplicity))
				

			self.schedule = weft.utils.createConcreteSchedule(actions, multiplicities)

			logger.debug('Completed schedule generation')

		elif self.executor.name == 'pn':
			# weft.utils.setupTwistedChannels(self.composite)
			# Start action processes
			weft.utils.setupActionProcesses(self.composite)
			print 'PN setup'
			print 'Starting reactor...'
			# twisted.internet.reactor.start()
			reactor.run()
			print 'Started reactor.'
		else:
			assert False

	def wrapup(self):
		super(Workflow, self).wrapup()
		# Close reactor
		print 'Stopping reactor...'
		# twisted.internet.reactor.stop()
		reactor.stop()
		print 'Reactor stopped.'

	def run(self):
		logger.debug('Preparing to run workflow "{}"'.format(self.name))
		self.composite.setup()
		while self.composite.wantsToRun():
			self.composite.run()
		logger.debug('Execution of workflow "{}" completed'.format(self.name))

	def hasStaticSchedule(self):
		return self.schedule is not None

	def getStaticSchedule(self):
		return self.schedule
