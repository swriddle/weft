import os
import tempfile
import weft
import yaml

def parseWorkflowFromYamlPath(filename):

	loadedYaml = '()'

	with open(filename, 'r') as f:
		loadedYaml = yaml.load(f)

	if loadedYaml is None:
		return None

	workflow = loadedYaml['workflow']

	composite, workflowName, executorName = workflow['composite'], workflow['name'], workflow['executor']

	executor = weft.utils.getExecutor(executorName)

	# print 'Workflow name: "{}"'.format(workflowName)

	channels, actions, compositeName = composite['channels'], composite['actions'], composite['name']

	# print 'Composite name: "{}"'.format(compositeName)

	topLevelComposite = weft.containers.Composite(compositeName)

	actionMap = {}

	for action in actions:
		name, cls = action['name'], action['class']
		args = action['args'] if 'args' in action else []
		clsObj = weft.utils.strToClass(cls)
		_args = [name] + args
		_action = clsObj(*_args)
		actionMap[name] = _action
		topLevelComposite.addAction(_action)
	
	for channel in channels:
		src, dst = channel['src'], channel['dst']
		srcName, srcPort = src['name'], src['port']
		dstName, dstPort = dst['name'], dst['port']
		srcAction, dstAction = actionMap[srcName], actionMap[dstName]
		topLevelComposite.addChannel((srcAction, srcPort), (dstAction, dstPort))

	workflow = weft.core.Workflow(workflowName, topLevelComposite, executor)

	return workflow

def generateGraphvizFromWorkflow(workflow):
	weftDir = os.environ['WEFTDIR']
	vizDir = os.path.join(weftDir, 'viz')
	fd, filename = tempfile.mkstemp(suffix='.dot', dir=vizDir)
	os.close(fd)
	template = 'digraph g {\n %s \n};'
	graphvizOutputContext = OutputContext()
	with open(filename, 'w') as f:
		id, graphviz = workflow.getGraphvizFor(graphvizOutputContext)
		f.write(template % (graphviz,))
	print 'Output complete to: {}'.format(filename)

class OutputContext:
	def __init__(self):
		self.highest = 0
		self.contents = {}

	def register(self, n):
		containmentString = n.getContainmentString()
		if containmentString in self.contents:
			pass
		else:
			val = self.highest
			assert val not in self.contents.values()
			self.highest += 1
			self.contents[containmentString] = val

		return self.contents[containmentString]
			
	pass
