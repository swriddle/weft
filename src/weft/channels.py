import collections


class Channel:
	def __init__(self):
		self.queue = collections.deque()

	def push(self, datum):
		self.queue.appendleft(datum)

	def pop(self):
		return self.queue.pop()

	def peek(self):
		return self.queue[-1]
