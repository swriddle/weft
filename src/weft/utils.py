import logging
import numpy
import os
import sys
from twisted.internet import protocol, reactor
import types

import weft


logger = logging.getLogger(__name__)

consumptionGlobal = weft.config.ConsumptionAttr
productionGlobal = weft.config.ProductionAttr

def strToClass(className):
	parts = className.split('.')
	current = None
	for part in parts:
		if current is None:
			current = sys.modules[part]
		else:
			current = getattr(current, part)
	return current

def toposortAndComputeMultiplicities(composite):
	
	links = composite.links

	_assertUniqueNames(links)
	
	sortedActions = _toposort(links)
	multiplicities = _computeMultiplicities(links)

	return sortedActions, multiplicities

# For each link (j, k), we know that to balance:
# prod_j * mult_j = cons_k * mult_k

def _toposort(links):

	actionLinks = set()

	for ((srcAction, srcPort), (dstAction, dstPort)) in links:
		actionLinks.add((srcAction, dstAction))

	sortedEdges = []

	while len(actionLinks) > 0:
		# get next layer - those things j where nothing in g at this point is incoming to an edge.
		dsts = set()

		for (src, dst) in actionLinks:
			dsts.add(dst)

		# Get all edges where the source is not the dst of any edge
		j = [(x, y) for (x, y) in actionLinks if x not in dsts]
		actionLinks = [(x, y) for (x, y) in actionLinks if (x, y) not in j]
		sortedEdges.extend(j)

	# Get the nodes from the sorted edges
	sortedNodes = []

	for (src, dst) in sortedEdges:
		if src not in sortedNodes:
			sortedNodes.append(src)

	for (src, dst) in sortedEdges:
		if dst not in sortedNodes:
			sortedNodes.append(dst)

	return sortedNodes




def _assertUniqueNames(links):

	actionLinks = set()

	# for ((srcAction, srcPort), (dstAction, dstPort)) in links:
	# 	actionLinks.add((srcAction, dstAction))

	actions = set()
	for ((srcAction, srcPort), (dstAction, dstPort)) in links:
		actions.add(srcAction)
		actions.add(dstAction)

	# Checking for time uniqueness so I can use action names for unique
	# hashable 'handles' for the action instances themselves.
	actionNames = set()
	for action in actions:
		actionNames.add(action.name)

	assert len(actions) == len(actionNames)






def _computeMultiplicities(links):

	actionLinks = set()

	for ((srcAction, srcPort), (dstAction, dstPort)) in links:
		actionLinks.add((srcAction, dstAction))

	actions = set()
	for action in actionLinks:
		actions.update(action)

	constraints = []

	for ((srcAction, srcPort), (dstAction, dstPort)) in links:
		actionLinks.add((srcAction, dstAction))
		productionRate = srcAction.getProductionRate(srcPort)
		consumptionRate = dstAction.getConsumptionRate(dstPort)

		if productionRate is None:
			raise Exception('"{}"->"{}" has no declared production rate'.format(srcAction.name, srcPort))
		if consumptionRate is None:
			raise Exception('"{}"->"{}" has no declared consumption rate'.format(dstAction.name, dstPort))
		if productionRate is not None and consumptionRate is not None:
			# m(src) * productionRate = m(dst) * consumptionRate
			constraints.append((srcAction, productionRate, dstAction, consumptionRate))

	multiplicities = balance(constraints)

	if multiplicities is None:
		logger.warning('Could not compute integral schedule. Perhaps the upper bound of the factor used for inflation is too low.')
		sys.exit(1)
	else:
		multiplicities = integerize(multiplicities)
		return multiplicities


	return multiplicities




def integerize(m):
	return dict([(x, int(y)) for (x, y) in m.items()])

def balance(constraints):

	# stringify, just for my own brain:
	# constraints = [(w.name, x, y.name, z) for (w, x, y, z) in constraints]

	# How many actions are there to determine multiplicities?
	
	actions = set()
	for (src, prodRate, dst, consRate) in constraints:
		actions.add(src)
		actions.add(dst)

	actionsList = list(actions)
	actionsCount = len(actionsList)
	positions = {}
	logger.debug('Enumerating all actions in workflow:')
	for (i, action) in enumerate(actionsList):
		positions[action] = i
		logger.debug('{}. "{}"'.format(i, action))

	# This could be done in one for-loop, but that would be more complicated.
	# As long as the workflow isn't HUGE, two for-loops shouldn't be a problem.

	answers = [0] * len(constraints)
	# Everything is relative. Got to break this tie arbitrarily.
	answers.append(1)

	rows = []
	logger.debug('Creating stoichiometry matrix now for {} constraints'.format(len(constraints)))
	for (src, prodRate, dst, consRate) in constraints:
		srcPos = positions[src]
		dstPos = positions[dst]
		row = [0] * actionsCount
		row[srcPos] = prodRate
		row[dstPos] = -consRate
		rows.append(row)
		pass
	row = [0] * len(constraints)
	row.append(1)
	rows.append(row)

	a = numpy.array(rows)

	b = numpy.array(answers)

	multiplicities = numpy.linalg.solve(a, b)

	actionsMult = dict(zip(actionsList, multiplicities))

	# Trying to lock on to the minimal multiplicative factor that makes all
	# integral.

	integralFactors = None

	for factor in range(2, 10):
		scaledActionsMult = dict([(x, y * factor) for (x, y) in actionsMult.items()])
		if almostIntegral(scaledActionsMult):
			integralFactors = scaledActionsMult
			break

	
	return integralFactors

		
	# for (src, prodRate, dst, consRate) in constraints:
    #
	# 	# productionRate * srcMult = consumptionRate * dstMult
	# 	
	# 	# srcMult = (consumptionRate * dstMult) / productionRate
	# 	# dstMult = (productionRate * srcMult) / consumptionRate
    #
	# 	pass
	# return cons

def createConcreteSchedule(actions, multiplicities):
	schedule = []
	for action in actions:
		schedule.extend([action] * multiplicities[action])
	return schedule


def almostIntegral(factors):
	return all([_almostIntegral(x) for x in factors.values()])


def _almostIntegral(n):
	# Will this work?
	return n.is_integer()


def reduce(n, d):
	g = gcd(n, d)
	n /= g
	d /= g
	# n and d are reduced
	return n, d
	pass

def getExecutor(executorName):
	if executorName == 'SDF':
		return weft.executors.SynchronousExecutor()
	elif executorName == 'PN':
		return weft.executors.ProcessNetworkExecutor()
	else:
		assert False

def setupActionProcesses(composite):
	processManager = weft.process.ProcessManager()
	print 'Got process manager:', id(processManager)
	for action in composite.getDeepActions():
		print 'Would setup: "{}"'.format(action.getContainmentString())
		protocol = TestProtocol(action)
		executable = '/usr/bin/cat'
		print 'Spawning process:'
		process = reactor.spawnProcess(protocol, executable, args=['cat', '/home/sean/helloworld.txt', '-'], env={'HOME':os.environ['HOME']}, path='/home/sean')
		print 'Spawn process.'
		processManager.addActionProcess(action, process)
		pass

def setupTwistedChannels(composite):
	for action in composite.getDeepActions():
		print 'Would setup: "{}"'.format(action.getContainmentString())
		pass

	for ((srcAction, srcPort), (dstAction, dstPort)) in composite.links:
		srcPortRef = srcAction.getContainmentString() + '.' + srcPort
		dstPortRef = dstAction.getContainmentString() + '.' + dstPort
		print 'Would link: {} -> {}'.format(srcPortRef, dstPortRef)

	assert False

def gcd(a, b):
	if b == 0:
		return a
	else:
		return gcd(b, a % b)

class TestProtocol(protocol.ProcessProtocol):
	def __init__(self, action):
		self.action = action

	def connectionMade(self):
		self.transport.write('Actor is named: "{}"'.format(self.action.name))
		self.transport.closeStdin()

	def outReceived(self, data):
		print 'I received data: {}'.format(data)
		print 'connectionMade!'
	pass
