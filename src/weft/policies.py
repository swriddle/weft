TO_ID = {}
FROM_ID = {}
LAST_ID = 0

def createChannelIdFromEndpoints(u, v):
	global LAST_ID
	h = hash(u)
	i = hash(v)
	k = h, i
	thisId = LAST_ID
	LAST_ID += 1
	TO_ID[k] = thisId
	FROM_ID[thisId] = k
	return thisId
