import logging
import Queue
import sys
import twisted
import weft

logger = logging.getLogger(__name__)

confirmAddChannel = weft.config.ConfirmAddChannel


class Composite(weft.core.Action):
	
	def __init__(self, name):
		super(Composite, self).__init__(name)
		self.nodes = []
		self.inputs = {}
		self.outputs = {}
		self.channels = {}
		self.attrs = {}
		self.links = []
		self.reactor = None

	def getExecutor(self):
		return self['parent'].getExecutor()

	def addAction(self, action):
		action['parent'] = self
		self.nodes.append(action)

	def isComposite(self):
		return True

	def getGraphvizFor(self, context):
		topLevelTemplate = 'subgraph %s { %s \n label="%s"; }'
		edgeTemplate = '%s -> %s [ headlabel="%s", taillabel="%s" ];'
		subparts = []
		compositeId = context.register(self)
		for node in self.nodes:
			internalId, internalGraphviz = node.getGraphvizFor(context)
			subparts.append(internalGraphviz)

		for ((srcAction, srcPort), (dstAction, dstPort)) in self.links:
			srcId, dstId = context.register(srcAction), context.register(dstAction)
			# edgeGraphviz = '%s -> %s { headlabel=%s, taillabel=%s };'
			edgeGraphviz = edgeTemplate % (srcId, dstId, srcPort, dstPort)
			subparts.append(edgeGraphviz)

		allInternalGraphviz = '\n'.join(subparts)

		graphvizOutput = topLevelTemplate % (compositeId, allInternalGraphviz, self.name)

		return compositeId, graphvizOutput
			

	def setup(self):
		super(Composite, self).setup()


		# Setup all actions
		for action in self.nodes:
			action.setup()
		
	def addChannel(self, u, v):

		srcAction, srcPort = None, None
		dstAction, dstPort = None, None
		logger.debug('Adding channel...')
		if confirmAddChannel:
			# Before adding, confirm that u is an exported output (action,
			# outputName) from some action and that v is an exported input
			# (action, inputName) from some action
			srcAction, srcPort = u
			dstAction, dstPort = v

			assert srcAction.hasOutput(srcPort)
			assert dstAction.hasInput(dstPort)

			assert srcAction in self
			assert dstAction in self

		newChannelId = weft.policies.createChannelIdFromEndpoints(u, v)
		self.channels[newChannelId] = weft.channels.Channel()
		self.inputs[u] = newChannelId
		self.outputs[v] = newChannelId
		self.links.append((u, v))

	def run(self):
		super(Composite, self).run()

		workflow = self['parent']
		executor = workflow.executor

		if executor.name == 'sdf':
			assert workflow.hasStaticSchedule()
			for action in workflow.getStaticSchedule():
				logger.debug('Executing action:', action.name)
				action.run()
		elif executor.name == 'pn':
			print 'PN run call!'
			pass
		else:
			raise Exception('WARNING: Unknown executor: "{}"'.format(executor.name))
			# print 'WARNING: Static schedule not found'

	def __len__(self):
		return len(self.nodes)

	def __setitem__(self, k, v):
		self.attrs[k] = v

	def __getitem__(self, k):
		return self.attrs[k]

	def __delitem__(self, k):
		raise NotImplementedError
		# del self.nodes[k]

	def __contains__(self, item):
		return item in self.nodes

	def __missing__(self, k):
		raise NotImplementedError
		# return None

	def send(self, action, portName, datum):
		channelId = self.inputs[(action, portName)]
		channel = self.channels[channelId]
		channel.push(datum)
		executorName = self.getExecutor().name
		if executorName == 'sdf':
			print 'sdf send'
		elif executorName == 'pn':
			print 'pn send'
		else:
			print 'unknown executor send: "{}"'.format(executorName)
		

	def receive(self, action, portName):
		channelId = self.outputs[(action, portName)]
		channel = self.channels[channelId]

		executorName = self.getExecutor().name
		if executorName == 'sdf':
			print 'sdf recv'
		elif executorName == 'pn':
			print 'pn recv'
		else:
			print 'unknown executor recv: "{}"'.format(executorName)

		return channel.pop()

	def peek(self, action, portName):
		channelId = self.outputs[(action, portName)]
		channel = self.channels[channelId]

		return channel.peek()
	

	def getDeepActions(self):
		actions = []
		queue = Queue.Queue()
		for node in self.nodes:
			queue.put(node)

		while not queue.empty():
			cur = queue.get()
			if cur.isComposite():
				for node in cur.nodes:
					queue.put(node)
			actions.append(cur)
		return actions

	# # Attempt to use a channel in the current context to send datum on portName
	# def send(self, portName, datum):
	# 	pass
    #
	# # Returns the next piece of data on the channel in the current context
	# # received on portName
	# def receive(self, portName):
	# 	pass
