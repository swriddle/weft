import logging
import weft

moduleLogger = logging.getLogger(__name__)

consumptionGlobal = weft.config.ConsumptionAttr
productionGlobal = weft.config.ProductionAttr

displayHandler = logging.StreamHandler()
displayHandler.setLevel(logging.INFO)

displayFormatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

displayHandler.setFormatter(displayFormatter)

class Constant(weft.core.Action):
	def __init__(self, name, const, isolated=False):
		super(Constant, self).__init__(name, isolated=isolated)
		self.const = const
		self.addOutputPort('output')
		self[productionGlobal].update({'output':1})

	def setup(self):
		super(Constant, self).setup()

	def run(self):
		super(Constant, self).run()
		self.send('output', self.const)

class SinglePortMultiplier(weft.core.Action):
	def __init__(self, name, isolated=False):
		super(SinglePortMultiplier, self).__init__(name, isolated=isolated)
		self.addInputPort('input')
		self.addOutputPort('output')
		self[consumptionGlobal].update({'input':2})
		self[productionGlobal].update({'output':1})

	def setup(self):
		super(SinglePortMultiplier, self).setup()

	def run(self):
		super(SinglePortMultiplier, self).run()
		u = self.receive('input')
		v = self.receive('input')
		w = u * v
		self.send('output', w)

class Adder(weft.core.Action):
	def __init__(self, name, isolated=False):
		super(Adder, self).__init__(name, isolated=isolated)
		self.addInputPort('input1')
		self.addInputPort('input2')
		self.addOutputPort('output')
		self[consumptionGlobal].update({'input1':1, 'input2':1})
		self[productionGlobal].update({'output':1})

	def setup(self):
		super(Adder, self).setup()

	def run(self):
		super(Adder, self).run()
		u = self.receive('input1')
		v = self.receive('input2')
		w = u + v
		self.send('output', w)

class Display(weft.core.Action):
	def __init__(self, name, isolated=False):
		super(Display, self).__init__(name, isolated=isolated)
		self.addInputPort('input')
		self[consumptionGlobal].update({'input':1})
		self.logger = None

	def setup(self):
		super(Display, self).setup()
		# Figure out the logger associated with this.
		containmentString = self.getContainmentString()
		self.logger = logging.getLogger(containmentString)
		self.logger.setLevel(logging.INFO)
		self.logger.addHandler(displayHandler)

	def run(self):
		super(Display, self).run()
		val = self.receive('input')
		string = 'DISPLAY "{}", iteration {}: "{}"'.format(self.name, self.currentIteration, val)
		if self.logger is None:
			moduleLogger.warn(string)
		else:
			self.logger.info(string)

# No declared token production/consumption rates; this is designed to be used
# with PN executor, not SDF.
class PRNGSortedMerge(weft.core.Action):
	def __init__(self, name, isolated=False):
		super(PRNGSortedMerge, self).__init__(name, isolated=isolated)
		self.addInputPort('input1')
		self.addInputPort('input2')
		self.addOutputPort('output')

	def setup(self):
		super(PRNGSortedMerge, self).setup()
		pass

	def run(self):
		super(PRNGSortedMerge, self).run()
		a, b = self.peek('input1'), self.peek('input2')
		if a < b:
			# Unambiguously a
			pass
		elif b < a:
			# Unambiguously b
			pass
		elif a == b:
			# Flip a coin
			pass
		else:
			assert False
		pass
