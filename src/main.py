import logging
import sys
import weft

logger = logging.getLogger(__name__)

def main(args):
	# workflow = createSimpleTestWorkflow()
	# workflow = weft.io.parseWorkflowFromYamlPath('/home/sean/weft/workflows/test-workflow.yaml')
	workflow = weft.io.parseWorkflowFromYamlPath('/home/sean/weft/workflows/hamming.yaml')
	runWorkflow(workflow)
	weft.io.generateGraphvizFromWorkflow(workflow)

def runWorkflow(workflow):
	logger.info('About to setup workflow...')
	workflow.setup()
	logger.info('Workflow setup complete')

	logger.info('About to run workflow...')
	workflow.run()
	logger.info('Workflow run complete')

def createSimpleTestWorkflow():
	
	composite = weft.containers.Composite('main-composite')

	const1 = weft.atomics.Constant('Constant1', 5)
	const2 = weft.atomics.Constant('Constant2', 4)
	adder = weft.atomics.Adder('Adder')
	multiplier = weft.atomics.SinglePortMultiplier('Multiplier1')
	
	display = weft.atomics.Display('Display1')

	composite.addAction(const1)
	composite.addAction(const2)
	composite.addAction(adder)
	composite.addAction(multiplier)
	composite.addAction(display)

	composite.addChannel((const1, 'output'), (adder, 'input1'))
	composite.addChannel((const2, 'output'), (adder, 'input2'))
	composite.addChannel((adder, 'output'), (multiplier, 'input'))
	composite.addChannel((multiplier, 'output'), (display, 'input'))

	workflow = weft.core.Workflow(composite, 'main-workflow')

	return workflow
	

if __name__ == '__main__':
	main(sys.argv)
