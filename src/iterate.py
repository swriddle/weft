# import cProfile
import sys

import weft

def main(args):
	try:
		className = args[1]
		actionName = args[2]
		actionArgs = args[3:]
		run(className, actionName, actionArgs)
	except ValueError:
		print 'Usage: python2 {} actorClassName'.format(args[0])
	print 'iterate script'

def run(className, actionName, args):
	cls = weft.utils.strToClass(className)
	action = cls(actionName)
	print 'Declared action.'
	print 'Args:', args
	# input = raw_input()
	# cls.


if __name__ == '__main__':
	# cProfile.run('__import__(\'weft\')')
	# cProfile.run('main(sys.argv))
	main(sys.argv)
